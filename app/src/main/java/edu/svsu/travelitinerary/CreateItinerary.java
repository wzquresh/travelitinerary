package edu.svsu.travelitinerary;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CreateItinerary extends Activity {

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    ItemListAdapter adapter;
    LinearLayout addToPacklist;
    LinearLayout toDoList;
    List<EditText> etList = new ArrayList<EditText>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_itinerary);

//        ArrayAdapter test = new ArrayAdapter(this, R.layout.activity_create_itinerary);
//        ListView testList = (ListView) findViewById(R.id.test);
//        testList.setAdapter(test);

        adapter = new ItemListAdapter(CreateItinerary.this, R.layout.item_to_pack, new ArrayList<Items>());
        addToPacklist = (LinearLayout) findViewById(R.id.AddToPacklist);
        toDoList = (LinearLayout) findViewById(R.id.toDoList);
        //addToPacklist.setAdapter(adapter);

        Button homeButton = (Button)findViewById(R.id.Home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateItinerary.this, SelectItinerary.class);
                startActivity(intent);
            }
        });

    }

    public void SubmitItinerary(View v){
        HashMap<String, String> top = new HashMap<>();
        ArrayList<String> underItinerary = new ArrayList<>();
        HashMap<String, String> underToPack = new HashMap<>();
        HashMap<String, String> underToDo = new HashMap<>();
        //HashMap<String, String> itemNameText = new HashMap<>();

        DateFormat format = new SimpleDateFormat("dd MM YYYY");


        Intent intent = new Intent(CreateItinerary.this, SelectItinerary.class);

        //Send to Firebase and Go to Select Itinerary Page
        EditText itineraryName = (EditText) findViewById(R.id.itineraryName);

        top.put("Image", "2131165282");

        //itemNameText.put("Name", itineraryName.getText().toString());
        underItinerary.add("To Pack");
        underItinerary.add("To Do");
        LinearLayout packList = (LinearLayout) findViewById(R.id.AddToPacklist);
        List<String> toPack = new ArrayList<String>();
        for(int i = 0; i < packList.getChildCount(); i++){
            //String item = etList.get(i).getText().toString();
            LinearLayout llitem = (LinearLayout) packList.getChildAt(i);
            EditText itemText = (EditText) llitem.getChildAt(0);
            String item = itemText.getText().toString();
            underToPack.put(String.valueOf(i), item);
            toPack.add(item);
        }
        Log.e("Pack List", "SubmitItinerary: " + underToPack );

        LinearLayout toDoList = (LinearLayout) findViewById(R.id.toDoList);
        HashMap<String,String> toDo = new HashMap<>();
        ArrayList<String> dates = new ArrayList<>();
        ArrayList<String> stuff = new ArrayList<>();
        for(int j = 0; j < toDoList.getChildCount(); j++){
            LinearLayout ll = (LinearLayout) toDoList.getChildAt(j);
            EditText dateText = (EditText) ll.getChildAt(0);
            EditText info = (EditText) ll.getChildAt(1);
            String text = dateText.getText().toString();
            String date = null;
            try{
                date = format.parse(text).toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            dates.add(text);
            stuff.add(info.getText().toString());
            toDo.put(date+j, info.getText().toString());
            underToDo.put(date+j, info.getText().toString());
        }

        HashMap<String, String> data = new HashMap<>();
        String toPackListStr = android.text.TextUtils.join(",", toPack);
        String toDatesStr = android.text.TextUtils.join(",", dates);
        String toDoListStr = android.text.TextUtils.join(",", stuff);
        data.put("Image", "2131165282");
        data.put("To Pack", toPackListStr);
        data.put("Dates", toDatesStr);
        data.put("To Do", toDoListStr);
        //Push data to database under Trip Name
        ref.child(itineraryName.getText().toString()).setValue(data);

        Log.e("Trip Data", "SubmitItinerary: " + data );

        Toast.makeText(getApplicationContext(), "New Itinerary Created!", Toast.LENGTH_SHORT ).show();
        startActivity(intent);
    }

    public void AddItemButton(View v){
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        EditText item = new EditText(this);
        item.setHint("Socks");
        Button delete = new Button(this);
        delete.setText("Delete");
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToPacklist.removeView((View) view.getParent());
            }
        });
        ll.addView(item);
        ll.addView(delete);

        addToPacklist.addView(ll);
    }
    public void AddToDo(View v){
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);

        EditText calDate = new EditText(this);
        calDate.setHint("MM/DD/YYYY");
        calDate.setInputType(InputType.TYPE_CLASS_DATETIME);

        EditText item = new EditText(this);
        item.setHint("Go for a picnic in the park");
        item.setMaxWidth(250);

        Button delete = new Button(this);
        delete.setText("Delete");
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDoList.removeView((View) view.getParent());
            }
        });
        ll.addView(calDate);
        ll.addView(item);
        ll.addView(delete);
        toDoList.addView(ll);
    }
}
