package edu.svsu.travelitinerary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class InfoSelector extends Activity {

    ArrayList<String> names = new ArrayList<>();
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();

    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_selector);

        final int id = (Integer) getIntent().getExtras().get("ITINERARY");
        names = getIntent().getStringArrayListExtra("LIST");
        Log.e("SELECT NAME", "onCreate: " + names.get(id) );
        //final DatabaseReference listRef = ref.child(names.get(id));

        title = (TextView) findViewById(R.id.itNameInfoSelect);
        title.setText(names.get(id));
        final DatabaseReference lineItem = ref.child(names.get(id));

        Button openToDo = (Button) findViewById(R.id.openToDo);
        openToDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InfoSelector.this, Schedule.class);
                intent.putExtra("NAME", names.get(id));
                startActivity(intent);
            }
        });

        Button openToPack = (Button) findViewById(R.id.openToPack);
        openToPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InfoSelector.this, ToPack.class);
                intent.putExtra("NAME", names.get(id));
                startActivity(intent);
            }
        });

        Button delete = (Button) findViewById(R.id.DeleteItinerary);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InfoSelector.this, SelectItinerary.class);
                lineItem.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().removeValue();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                Toast.makeText(getApplicationContext(), names.get(id)+" Itinerary Deleted!", Toast.LENGTH_SHORT ).show();
                startActivity(intent);
            }
        });


        Button homeButton = (Button)findViewById(R.id.Home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InfoSelector.this, SelectItinerary.class);
                intent.putExtra("NAME", names.get(id));
                startActivity(intent);
            }
        });
    }
}
