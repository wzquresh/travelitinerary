package edu.svsu.travelitinerary;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

/**
 * Created by waqas on 12/10/2017.
 */

public class ItemListAdapter extends ArrayAdapter<Items> {

    private List<Items> items;
    private int resourceId;
    private Context context;

    public ItemListAdapter(Context context, int ResourceId, List<Items> items){
        super(context, ResourceId, items);
        this.context = context;
        this.resourceId = ResourceId;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View row = convertView;
        ItemHolder holder = null;

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(resourceId, parent, false);
        holder = new ItemHolder();
        holder.items = items.get(position);
        holder.editItem = (EditText)row.findViewById(R.id.itemToPack);
        holder.delete = (Button)row.findViewById(R.id.DeleteItem);
        holder.delete.setTag(holder.delete);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                items.remove(position);
                notifyDataSetChanged();
            }
        });

        row.setTag(holder);

        return row;
    }

    public static class ItemHolder{
        Items items;
        EditText editItem;
        Button delete;
    }
}
