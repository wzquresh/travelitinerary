package edu.svsu.travelitinerary;

import java.io.Serializable;

/**
 * Created by waqas on 12/10/2017.
 */

public class Items implements Serializable {

    private String name = "";
    private double value = 0;

    public Items(String name, double value){
        this.setName(name);
        this.setValue(value);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
