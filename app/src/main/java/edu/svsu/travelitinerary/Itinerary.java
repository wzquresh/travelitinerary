package edu.svsu.travelitinerary;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by waqas on 12/10/2017.
 */

public class Itinerary {

    HashMap<String, String> top;
    ArrayList<String> underItinerary;
    HashMap<String, String> underToPack;
    HashMap<String, String> underToDo;


    public ArrayList<String> packList;
    public HashMap<Date,HashMap<String, String>> schedule;
    public HashMap<String, String> day;

    public Itinerary(){

    }
    public Itinerary(ArrayList<String> toPack, HashMap<Date, HashMap<String, String>> schedule){
        this.packList = toPack;
        this.schedule = schedule;
        for (HashMap<String, String> item : schedule.values()) {
            this.day.putAll(item);
        }
    }

    public ArrayList<String> getPackList() {
        return packList;
    }

    public void setPackList(ArrayList<String> packList) {
        this.packList = packList;
    }

    public HashMap<Date, HashMap<String, String>> getSchedule() {
        return schedule;
    }

    public void setSchedule(HashMap<Date, HashMap<String, String>> schedule) {
        this.schedule = schedule;
    }

    public HashMap<String, String> getDay() {
        return day;
    }

    public void setDay(HashMap<String, String> day) {
        this.day = day;
    }
}
