package edu.svsu.travelitinerary;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by waqas on 12/13/2017.
 */

public class ItineraryListAdapter extends BaseAdapter {

    ArrayList itInfo;
    Context context;
    LayoutInflater cellInflater;
    ItineraryListModel temp = null;

    public ItineraryListAdapter(ArrayList a, Context context){

        itInfo = a;
        this.context = context;
        cellInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return itInfo.size();
    }

    @Override
    public Object getItem(int i) {
        return itInfo.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public static class ViewHolder{
        public ImageView itineraryImage;
        public TextView itineraryLabel;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        ViewHolder holder;

        if(view == null){
            vi = cellInflater.inflate(R.layout.itinerary_item, viewGroup, false);
            holder = new ViewHolder();
            holder.itineraryImage = (ImageView) vi.findViewById(R.id.ItineraryImage);
            holder.itineraryLabel = (TextView) vi.findViewById(R.id.ItineraryItem);
            vi.setTag(holder);
        }else{
            holder = (ViewHolder) vi.getTag();
        }
        if(itInfo.size()<=0){
            holder.itineraryLabel.setText("No Data");
        }else{
            temp = null;
            temp = (ItineraryListModel) itInfo.get(i);
            Log.e("INFO", "getView: " + temp.getItineraryName());
            holder.itineraryLabel.setText(temp.getItineraryName());
            Log.e("Image", "getView: " + R.drawable.disney);
            holder.itineraryImage.setImageResource(temp.getImageResourceId());
        }


        return vi;
    }
}
