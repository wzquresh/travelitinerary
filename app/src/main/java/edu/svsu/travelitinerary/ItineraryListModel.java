package edu.svsu.travelitinerary;

/**
 * Created by waqas on 12/13/2017.
 */

public class ItineraryListModel {
    int imageResourceId;
    String itineraryName;

    public ItineraryListModel(int imageId, String itineraryName){
        this.imageResourceId = imageId;
        this.itineraryName = itineraryName;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public String getItineraryName() {
        return itineraryName;
    }

    public void setItineraryName(String itineraryName) {
        this.itineraryName = itineraryName;
    }
}
