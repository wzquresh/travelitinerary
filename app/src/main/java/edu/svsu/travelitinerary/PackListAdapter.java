package edu.svsu.travelitinerary;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by waqas on 12/15/2017.
 */

public class PackListAdapter extends BaseAdapter {

    ArrayList a;
    Context context;
    LayoutInflater cellInflater;
    PackListModel temp = null;

    public PackListAdapter(ArrayList a, Context context) {
        this.a = a;
        this.context = context;
        cellInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return a.size();
    }

    @Override
    public Object getItem(int i) {
        return a.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public static class ViewHolder{
        public TextView packedItem;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        ViewHolder holder;

        if(view == null){
            vi = cellInflater.inflate(R.layout.item_packed, viewGroup, false);
            holder = new PackListAdapter.ViewHolder();
            holder.packedItem = (TextView) vi.findViewById(R.id.PackedItem);
            vi.setTag(holder);
        }else{
            holder = (PackListAdapter.ViewHolder) vi.getTag();
        }
        if(a.size()<=0){
            holder.packedItem.setText("No Data");
        }else{
            temp = null;
            temp = (PackListModel) a.get(i);
            holder.packedItem.setText(temp.getItemToPack());
        }

        return vi;
    }
}
