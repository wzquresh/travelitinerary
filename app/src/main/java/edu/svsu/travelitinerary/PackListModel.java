package edu.svsu.travelitinerary;

/**
 * Created by waqas on 12/15/2017.
 */

public class PackListModel {
    String itemToPack;

    public PackListModel(String itemToPack) {
        this.itemToPack = itemToPack;
    }

    public String getItemToPack() {
        return itemToPack;
    }

    public void setItemToPack(String itemToPack) {
        this.itemToPack = itemToPack;
    }
}
