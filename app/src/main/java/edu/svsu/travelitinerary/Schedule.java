package edu.svsu.travelitinerary;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Schedule extends Activity {

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    ArrayList<ScheduleListModel> scheduleList = new ArrayList<>();
    ListView eventsList;
    ScheduleListAdapter adapter;

    ArrayList<String> dateList;
    ArrayList<String> infoList;

    TextView pageHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        pageHeader = (TextView) findViewById(R.id.pageHeader);

        Intent intent = getIntent();
        String name = intent.getStringExtra("NAME");
        pageHeader.setText("To Do In: " + name);

        eventsList = (ListView) findViewById(R.id.EventsList);
        adapter = new ScheduleListAdapter(scheduleList, this);
        eventsList.setAdapter(adapter);

        DatabaseReference nameRef = ref.child(name);
        nameRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.e("Schedule Snapshot", "onDataChange: " + dataSnapshot.getValue() );
                    scheduleList.clear();
                    final Map<String, String> lineItem = (HashMap<String,String>) dataSnapshot.getValue();
                    String dates = lineItem.get("Dates");
                    String stuff = lineItem.get("To Do");
                    dateList = new ArrayList<>(Arrays.asList(dates.split("\\s*,\\s*")));
                    infoList = new ArrayList<>(Arrays.asList(stuff.split("\\s*,\\s*")));
                    for(int i = 0; i < dateList.size(); i++){
                        ScheduleListModel model = new ScheduleListModel(dateList.get(i), infoList.get(i));
                        scheduleList.add(model);
                    }
                    eventsList.setAdapter(adapter);
                }else{
                    eventsList.setAdapter(null);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Button homeButton = (Button)findViewById(R.id.Home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Schedule.this, SelectItinerary.class);
                startActivity(intent);
            }
        });
    }
}
