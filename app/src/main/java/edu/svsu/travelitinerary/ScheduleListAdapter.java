package edu.svsu.travelitinerary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by waqas on 12/15/2017.
 */

public class ScheduleListAdapter extends BaseAdapter {

    ArrayList a;
    Context context;
    LayoutInflater cellInflater;
    ScheduleListModel temp = null;

    public ScheduleListAdapter(ArrayList a, Context context) {
        this.a = a;
        this.context = context;
        cellInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return a.size();
    }

    @Override
    public Object getItem(int i) {
        return a.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public static class ViewHolder{
        public TextView dateTextView;
        public TextView infoTextView;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        ViewHolder holder;

        if(view == null){
            vi = cellInflater.inflate(R.layout.to_do_item, viewGroup, false);
            holder = new ScheduleListAdapter.ViewHolder();
            holder.dateTextView = (TextView) vi.findViewById(R.id.dateText);
            holder.infoTextView = (TextView) vi.findViewById(R.id.infoText);
            vi.setTag(holder);
        }else{
            holder = (ScheduleListAdapter.ViewHolder) vi.getTag();
        }
        if(a.size()<=0){
            holder.infoTextView.setText("No Data");
        }else{
            temp = null;
            temp = (ScheduleListModel) a.get(i);
            holder.dateTextView.setText(temp.getDate());
            holder.infoTextView.setText(temp.getInfo());
        }

        return vi;
    }
}
