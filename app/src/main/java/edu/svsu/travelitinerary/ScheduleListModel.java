package edu.svsu.travelitinerary;

/**
 * Created by waqas on 12/15/2017.
 */

public class ScheduleListModel {

    String date, info;

    public ScheduleListModel(String date, String info) {
        this.date = date;
        this.info = info;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
