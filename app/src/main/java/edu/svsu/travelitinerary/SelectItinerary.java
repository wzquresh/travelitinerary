package edu.svsu.travelitinerary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectItinerary extends Activity {

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    ArrayList<ItineraryListModel> itList = new ArrayList<>();
    ArrayList<String> itNames = new ArrayList<>();
    ListView itineraryListView;
    ItineraryListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_itinerary);

        itineraryListView = (ListView)findViewById(R.id.itineraryList);
        adapter = new ItineraryListAdapter(itList, this);
        itineraryListView.setAdapter(adapter);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    itList.clear();
                    itNames.clear();
                    for(DataSnapshot data : dataSnapshot.getChildren()){
                        Log.e("Firebase Data", "onDataChange: " + data.getValue() );

                        final Map<String, String> lineItem = (HashMap<String,String>) data.getValue();
                        Log.e("Test", "onDataChange: " + String.valueOf(lineItem.get("Image")));
                        String image = String.valueOf(lineItem.get("Image"));
                        int id = Integer.valueOf(image);

                        String itinerary = data.getKey();
                        //final Map<String, String> name = (HashMap<String, String>) data.child("Itinerary").getValue();
                        //String itineraryName = name.get("Name");
                        ItineraryListModel mItem = new ItineraryListModel(id, itinerary);
                        itNames.add(itinerary);
                        itList.add(mItem);
                    }
                    itineraryListView.setAdapter(adapter);
                }else{
                    itineraryListView.setAdapter(null);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SelectItinerary.this, InfoSelector.class);
                intent.putExtra("ITINERARY", (int) l);
                intent.putStringArrayListExtra("LIST", itNames);
                startActivity(intent);
            }
        };

        itineraryListView.setOnItemClickListener(itemClickListener);

    }

    public void CreateItinerary(View v){
        Intent intent = new Intent(this, CreateItinerary.class);
        startActivity(intent);
    }
}
