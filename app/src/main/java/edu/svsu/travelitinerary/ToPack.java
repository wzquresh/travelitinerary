package edu.svsu.travelitinerary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ToPack extends Activity {

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    ArrayList<PackListModel> customList = new ArrayList<PackListModel>();
    ListView packListView;
    PackListAdapter adapter;

    ArrayList<String> packList;

    TextView packHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_pack);

        packListView = (ListView)findViewById(R.id.PackingList);
        adapter = new PackListAdapter(customList, this);
        packListView.setAdapter(adapter);

        Intent intent = getIntent();
        String name = intent.getStringExtra("NAME");
        packHeader = (TextView) findViewById(R.id.packHeader);
        packHeader.setText("To Pack For: " + name);

        final DatabaseReference nameRef = ref.child(name);

        nameRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    customList.clear();
                    final Map<String, String> packItem = (HashMap<String, String>) dataSnapshot.getValue();
                    String packItems = packItem.get("To Pack");
                    packList = new ArrayList<>(Arrays.asList(packItems.split("\\s*,\\s*")));
                    for(int i = 0; i < packList.size(); i++){
                        PackListModel model = new PackListModel(packList.get(i));
                        customList.add(model);
                    }
                    packListView.setAdapter(adapter);
                }else{
                    packListView.setAdapter(null);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Button homeButton = (Button)findViewById(R.id.Home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ToPack.this, SelectItinerary.class);
                startActivity(intent);
            }
        });
    }
}
